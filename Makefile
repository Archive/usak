all:	usak/usak.grdb

usak/usak.grdb:	input.gmif
#	rm -f usak/usak.grdb usak/usak.posdb
	./import --no-ht --no-db -f input.gmif
	./reorganize usak/usak

input.gmif:	prepare
	./prepare > $@
